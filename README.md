# Introduction

This project creates a docker image with jdk 11 and maven 3 installed.

The image can be used to run maven builds.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk11-maven3
