ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jdk11:latest

ARG VERSION=3.9.6
ARG BASE_URL=https://downloads.apache.org
ARG SHA=706f01b20dec0305a822ab614d51f32b07ee11d0218175e55450242e49d2156386483b506b3a4e8a03ac8611bae96395fd5eec15f50d3013d5deed6d1ee18224

ENV JAVA_HOME=/usr/lib/jvm/temurin-11-jdk-amd64

RUN <<EOF
apt-get update
apt-get -y upgrade
apt-get -y install curl
mkdir -p /usr/share/apache-maven
curl -sSL ${BASE_URL}/maven/maven-3/${VERSION}/binaries/apache-maven-${VERSION}-bin.tar.gz -o apache-maven-bin.tar.gz
echo "${SHA}  apache-maven-bin.tar.gz" | sha512sum -c
tar xzCf /usr/share/apache-maven apache-maven-bin.tar.gz --strip-components=1
ln -s /usr/share/apache-maven/bin/mvn /usr/bin
rm -f apache-maven-bin.tar.gz
EOF
